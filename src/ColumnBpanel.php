<?php

namespace Bittacora\DatatableBpanel;

use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class Column.
 */
class ColumnBpanel extends Column
{
    /**
     * @var null
     */
    protected $columnClass;

    /**
     * @return string
     */
    public function getColumnClass(): ?string
    {
        return $this->columnClass;
    }

    public function columnClass(string $class): self{
        $this->columnClass = $class;

        return $this;
    }
}

